module.exports = function (app) {
        var request = require('sync-request'); //used for synchronous requests
        var bodyparser = require('body-parser') //used to parse the client-request body
        app.use(bodyparser.json({ limit: '50mb', extended: true }))
        app.use(bodyparser.urlencoded({ limit: '50mb', extended: false }))

         //This is my homepage
        app.get('/',function(req,res) {
            res.render('home')
        })

        //Below functions returns  all the required details and it accepts url of repo,value of n,m as arguments 
        function getDetails(url,n,m) {
            var ans = []

            //Below is the GET request to GitHub API
            var ress = request('GET',url,{
                headers:{
                'accept':'application/vnd.github.v3+json',
                'user-agent': 'example-user-agent',
                'Authorization':'token e3fe41676ef0e30db344ec395cea40a77af0f365'
                }
            });
            var repos = JSON.parse(ress.getBody('utf8'));
            n = Math.min(n,repos.length) //n can't exceed total no of repos


            //Below is the custom sort function to sort repos on the basis of their forks
            repos.sort(function(x,y) {
                if(x.forks > y.forks) return -1;
                if(x.forks < y.forks) return 1;
                return 0;
            })

            //Below operations used to get top contributors for each repo
            repos.slice(0,n).forEach(async (e) => {
                //Below is the get req for getting top contributors
                    var us = request('GET',e.url + '/contributors',{
                        headers:{
                            'accept':'application/vnd.github.v3+json',
                            'user-agent': 'example-user-agent',
                            'Authorization':'token e3fe41676ef0e30db344ec395cea40a77af0f365'
                            }
                    });
                    var users = JSON.parse(us.getBody('utf8'));
                  //  res.send(users);
                    var temp  = {}
                    temp.repo = e.name;
                    temp.forks = e.forks
                    temp.contri = []
                    users.slice(0,m).forEach(ele => {
                        temp.contri.push({
                            "name":ele.login,
                            "commits":ele.contributions
                        })
                    })
                   // console.log(1); 
                    ans.push(temp)
            })
            return ans;
        }

        //API to display the result
        app.post('/getrepos',function(req,res) {
                    var org = req.body.org;
                    var n = req.body.n;
                    var m = req.body.m;
                    var url = `https://api.github.com/orgs/` +org + `/repos`
                    var ans = getDetails(url,n,m); 
                    res.render('show',{ans}); //Rendering UI and sending the data to  be displayed to the client
            })
            

}




