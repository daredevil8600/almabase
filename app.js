var express =  require('express'); // Used for api calls
var controller = require('./controllers/controller');//this is the controller of my web app

var app = express();
app.set('view engine','ejs'); //ejs will be my view engine to render UI
app.enable('trust proxy'); 
//app.use('/views')
app.use('/controllers',express.static('controllers'))
app.listen(5001,'localhost');
controller(app);