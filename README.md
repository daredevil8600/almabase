This app is also hosted on the below link:

https://almabasetask.herokuapp.com/



1. Clone the repository to your local machine

2. Install npm and node.js into your machine. Use link ( "https://nodejs.org/en/download/" )

3. Once installed and after extracting the cloned repo go to the repo folder

4. Run below command to install required dependencies

    $ npm install 

5. Now that all the dependencies are install we are ready to run the app

6. To run the app open terminal in the repo folder

7. Run $ node app.js command

8. If the port is busy, go to app.js and change the port to any availbale one

9. Here we go ..The app is up and running


